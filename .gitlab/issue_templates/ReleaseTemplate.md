# Internal Milestone X.y Review & Discussion :rocket: 
**X.y Milestone**: 2020-00-00 to 2020-00-00

[Plan Board](https://gitlab.com/groups/gitlab-org/-/boards/1489550?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=cicd%3A%3Aplanning&label_name[]=group%3A%3Aprogressive%20delivery)

[Schedule Board](https://gitlab.com/groups/gitlab-org/-/boards/1489554?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=workflow%3A%3Ascheduling)

[Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1489558?&label_name[]=cicd%3A%3Aactive&label_name[]=group%3A%3Aprogressive%20delivery)

## Theme and/or Goal :thinking_face:

<!-- add a few bullet items that can provide more context into why we're working on these issues -->

- Focus 1
- Focus 2
 
## Holidays :palm_tree:

Please order by _From_ date

| Person | From      | To      |
| ------ | --------- | ------- |
|        |           |         |

 
## Capacity :package:

Last 3 Milestones: @csouthard to fill out
* **Average MRs completed:** 

* **Average MRs per Engineer:** 

Stats derived from here: https://app.periscopedata.com/app/gitlab/638154/WIP-Group::Progressive-Delivery

<!--
For each section below, please add top issues in the following format:

* [Issue Title](https://gitlab.com/gitlab-org/gitlab/issues/xxxxx)

If no issues, please use 

* No issues currently listed :muscle:

--> 



## Deliverables :fox: with Spillover :repeat:
 
Top priority deliverables: 
<!-- Add Issues Here --> 

~Release::P1

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


~Release::P2

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


~Release::P3

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


**Total weight: XX**

#### Research :sleuth_or_spy: 
<!-- Add Issues Here --> 
- Needs Weight Issue -

## User Experience :roller_coaster: 
<!-- Add Issues Here -->

[workflow::design Issues](https://gitlab.com/groups/gitlab-org/-/boards/1486424?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=cicd%3A%3Aplanning&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=workflow%3A%3Adesign)

## Bug Fixes :bug: 
Top bugs:
<!-- Add Issues Here --> 

[Bug Issues](https://gitlab.com/groups/gitlab-org/-/boards/1489558?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=cicd%3A%3Aactive&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=bug)
 
| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


## Security Fixes :lock:
Top security Issues:
<!-- Add Issues Here --> 

[Security Issues](https://gitlab.com/groups/gitlab-org/-/boards/1489558?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=cicd%3A%3Aactive&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=security)

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


 
## Technical Debt :briefcase:
<!-- Add Issues Here --> 

[Technical Debt Issues](https://gitlab.com/groups/gitlab-org/-/boards/1489558?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=cicd%3A%3Aactive&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=technical%20debt)

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |


## User Experience Debt :paintbrush:
<!-- Add Issues Here --> 

[UX Debt Issues](https://gitlab.com/groups/gitlab-org/-/boards/1489558?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=UX%20debt)

| Issue | Weight | Note |
|-------|--------|------|
|       |        |      |
|       |        |      |

 
## OKR Alignment :dart:
<!-- Add Issues Here --> 


* [Progressive Delivery Quarterly goal](https://gitlab.com/groups/gitlab-org/-/epics/2636)
* Product Manager complete 2 Validation Cycles - https://gitlab.com/gitlab-com/Product/issues/730 
* Product Manager completed projected Maturity 
* Design OKR - Conduct 5 user research sessions for the UX Maturity Scorecard to evaluate the experience of Release Orchestration https://gitlab.com/gitlab-com/www-gitlab-com/issues/6201
* [Engineering OKRs (CI/CD)](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%2FCD%20Sub-Department&label_name[]=OKR)

## Things to Pay Attention To :eyes:
<!-- Add item --> 
* [Deprecate Legacy Feature Flags](https://gitlab.com/gitlab-org/gitlab/-/issues/209154) - if not done in 13.0, we will need to push this to 14.0
* [Move features to core: "Feature Flags"](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)

## Planning Tasks
* [ ] Team updates holidays section
* [ ] EM update capacity section
* [ ] PM highlights work for Deliverables, User Experience, Bug Fixes, Security Fixes, Technical Debt sections
* [ ] PM describes how this milestone aligns with our team's OKRs
* [ ] Team highlights Things to Pay Attention To section
* [ ] Engineers begin working on issues in `workflow::ready for development`

## Adjacent Planning Issues

[Previous](#) <=  => [Next](#)

/label ~"Planning Issue" ~"group::progressive delivery" ~"section::ops" ~"devops::release"
